<?php
namespace AppBundle\DBAL;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class EnumCompanyType extends Type
{
    const ENUM_COMPANY = 'enumcompany';
    const TYPE_IT = 'IT';
    const TYPE_SPORT_CLUB = 'sport club';
	const TYPE_CULTURAL_CENTER = 'cultural center';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "ENUM('IT','sport club','cultural center')";// COMMENT '(DC2Type:enumcompany)'
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!in_array($value, array(self::TYPE_IT, self::TYPE_SPORT_CLUB, self::TYPE_CULTURAL_CENTER))) {
            throw new \InvalidArgumentException("Invalid type of company");
        }
        return $value;
    }

    public function getName()
    {
        return self::ENUM_COMPANY;
    }
	
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}