<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;

class MoveEmployees
{
	protected $em;
	
	public function __construct(EntityManager $entityManager)
	{
		$this->em = $entityManager;
	}
	
    public function move(Request $request, int $companyId)
    {
		//$em = $this->getDoctrine()->getManager();
		$em = $this->em;
		
		$employees = $em->getRepository('AppBundle:Employee')->findByCompany($companyId);
		$company = $em->getRepository('AppBundle:Company')->findCompanyWithLeastEmployees();
		foreach($employees as $employee){
			$employee->setCompany($company);
            $em->persist($employee);
            $em->flush();		
		}
		
		//return $this->redirectToRoute('employees_index', array('id' => $companyId));
	}
}
