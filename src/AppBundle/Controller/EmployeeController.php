<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Employee;
use AppBundle\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Service\MoveEmployees;

/**
 * Employee controller.
 *
 * @Route("employees")
 */
class EmployeeController extends Controller
{
    /**
     * Lists all employee entities.
     *
     * @Route("/company/{id}", name="employees_index")
     * @Method("GET")
     */
    public function indexAction(Company $company)
    {
        $em = $this->getDoctrine()->getManager();

        //$employees = $em->getRepository('AppBundle:Employee')->findAll();
		//$employees = $em->getRepository('AppBundle:Employee')->findByCompany($id);
		$employees = $em->getRepository('AppBundle:Employee')->findAllWithLastAddress($company);
		
        return $this->render('employee/index.html.twig', array(
            'employees' => $employees,
			'company' => $company->getId(),
        ));
    }

    /**
     * Creates a new employee entity.
     *
     * @Route("/new/{company}", name="employees_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Company $company)
    {
        $em = $this->getDoctrine()->getManager();
		
		$employee = new Employee();
		//$company = $em->getReference('AppBundle\Entity\Company', $companyId);
		$employee->setCompany($company);
        $form = $this->createForm('AppBundle\Form\EmployeeType', $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($employee);
            $em->flush();

            return $this->redirectToRoute('employees_show', array('id' => $employee->getId()));
        }

        return $this->render('employee/new.html.twig', array(
            'employee' => $employee,
			'company' => $company->getId(),
            'form' => $form->createView(),
        ));
    }
	
	/**
     * Moves employee to the company with least employees.
     *
     * @Route("/move/{companyId}", name="employees_move")
     * @Method({"GET", "POST"})
     */
    public function moveAction(MoveEmployees $moveEmployees, Request $request, int $companyId)
    {
		$moveEmployees->move($request, $companyId);
		/*$em = $this->getDoctrine()->getManager();
		
		$employees = $em->getRepository('AppBundle:Employee')->findByCompany($companyId);
		$company = $em->getRepository('AppBundle:Company')->findCompanyWithLeastEmployees();
		foreach($employees as $employee){
			$employee->setCompany($company);
            $em->persist($employee);
            $em->flush();		
		}*/
		
		return $this->redirectToRoute('employees_index', array('id' => $companyId));
	}
	
	/**
     * Ajax switch active status.
     *
     * @Route("/switch/{id}", name="employees_switch")
     * @Method({"GET", "POST"})
     */
	public function switchActiveStatus(int $id)
    {
		$em = $this->getDoctrine()->getManager();
		
		$employee = $em->getRepository('AppBundle:Employee')->find($id);
		$employee->setIsActive(!$employee->getIsActive());
		$em->persist($employee);
        $em->flush();
		//return $employee->getIsActive();
		return new Response($employee->getIsActive() ? $this->get('translator')->trans('Yes') : $this->get('translator')->trans('No'));
	}

    /**
     * Finds and displays a employee entity.
     *
     * @Route("/{id}", name="employees_show")
     * @Method("GET")
     */
    public function showAction(Employee $employee)
    {
        $deleteForm = $this->createDeleteForm($employee);

        return $this->render('employee/show.html.twig', array(
            'employee' => $employee,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing employee entity.
     *
     * @Route("/{id}/edit", name="employees_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Employee $employee)
    {
        $deleteForm = $this->createDeleteForm($employee);
        $editForm = $this->createForm('AppBundle\Form\EmployeeType', $employee);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('employees_edit', array('id' => $employee->getId()));
        }

        return $this->render('employee/edit.html.twig', array(
            'employee' => $employee,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a employee entity.
     *
     * @Route("/{id}", name="employees_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Employee $employee)
    {
        $form = $this->createDeleteForm($employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
			/*$addresses = $employee->getAddresses();
			foreach($addresses as $address){
				$em->remove($address);
			}*/
            $em->remove($employee);
            $em->flush();
        }

        return $this->redirectToRoute('employees_index', array('id' => $employee->getCompany()->getId()));
    }

    /**
     * Creates a form to delete a employee entity.
     *
     * @param Employee $employee The employee entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Employee $employee)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('employees_delete', array('id' => $employee->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
