<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Address controller.
 *
 * @Route("addresses")
 */
class AddressController extends Controller
{
    /**
     * Lists all address entities.
     *
     * @Route("/employee/{id}", name="addresses_index")
     * @Method("GET")
     */
    public function indexAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();

        //$addresses = $em->getRepository('AppBundle:Address')->findAll();
		$addresses = $em->getRepository('AppBundle:Address')->findByEmployee($id);

        return $this->render('address/index.html.twig', array(
            'addresses' => $addresses,
			'employee' => $id,
        ));
    }

    /**
     * Creates a new address entity.
     *
     * @Route("/new/{employeeId}", name="addresses_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, int $employeeId)
    {
		$em = $this->getDoctrine()->getManager();
		
        $address = new Address();
		$employee = $em->getReference('AppBundle\Entity\Employee', $employeeId);
		$address->setEmployee($employee);
        $form = $this->createForm('AppBundle\Form\AddressType', $address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($address);
            $em->flush();

            return $this->redirectToRoute('addresses_show', array('id' => $address->getId()));
        }

        return $this->render('address/new.html.twig', array(
            'address' => $address,
			'employee' => $employeeId,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a address entity.
     *
     * @Route("/{id}", name="addresses_show")
     * @Method("GET")
     */
    public function showAction(Address $address)
    {
        $deleteForm = $this->createDeleteForm($address);

        return $this->render('address/show.html.twig', array(
            'address' => $address,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing address entity.
     *
     * @Route("/{id}/edit", name="addresses_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Address $address)
    {
        $deleteForm = $this->createDeleteForm($address);
        $editForm = $this->createForm('AppBundle\Form\AddressType', $address);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('addresses_edit', array('id' => $address->getId()));
        }

        return $this->render('address/edit.html.twig', array(
            'address' => $address,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a address entity.
     *
     * @Route("/{id}", name="addresses_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Address $address)
    {
        $form = $this->createDeleteForm($address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();			
            $em->remove($address);
            $em->flush();
        }

        return $this->redirectToRoute('addresses_index', array('id' => $address->getEmployee()->getId()));
    }

    /**
     * Creates a form to delete a address entity.
     *
     * @param Address $address The address entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Address $address)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('addresses_delete', array('id' => $address->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
