<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AddressRepository")
 */
class Address
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
     * @ORM\ManyToOne(targetEntity="Employee", inversedBy="addresses")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=100)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="house_number", type="string", length=10)
     */
    private $houseNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="post_number", type="integer", length=5)
     */
    private $postNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="post_office", type="string", length=100)
     */
    private $postOffice;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
	
	public function getEmployee()
    {
        return $this->employee;
    }
	
    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set houseNumber
     *
     * @param string $houseNumber
     *
     * @return Address
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * Get houseNumber
     *
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * Set postNumber
     *
     * @param integer $postNumber
     *
     * @return Address
     */
    public function setPostNumber($postNumber)
    {
        $this->postNumber = $postNumber;

        return $this;
    }

    /**
     * Get postNumber
     *
     * @return int
     */
    public function getPostNumber()
    {
        return $this->postNumber;
    }

    /**
     * Set postOffice
     *
     * @param string $postOffice
     *
     * @return Address
     */
    public function setPostOffice($postOffice)
    {
        $this->postOffice = $postOffice;

        return $this;
    }

    /**
     * Get postOffice
     *
     * @return string
     */
    public function getPostOffice()
    {
        return $this->postOffice;
    }
}

