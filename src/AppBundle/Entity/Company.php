<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
    /**
     * @ORM\OneToMany(targetEntity="Employee", mappedBy="company")
     */
    private $employees;

    public function __construct()
    {
        $this->employees = new ArrayCollection();
    }
	
	public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var int
     *
     * @ORM\Column(name="post_number", type="integer", length=5)
     */
    private $postNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="post_office", type="string", length=100)
     */
    private $postOffice;

    /**
     * @var int
     *
     * @ORM\Column(name="tax_number", type="integer", nullable=true)
     */
    private $taxNumber;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_tax_payer", type="boolean")
     */
    private $isTaxPayer = false;

    /**
     * @var string
     *
     * @ORM\Column(name="type_of_company", type="string", columnDefinition="ENUM('IT', 'sport club', 'cultural center')")
     */
    private $typeOfCompany;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Company
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postNumber
     *
     * @param integer $postNumber
     *
     * @return Company
     */
    public function setPostNumber($postNumber)
    {
        $this->postNumber = $postNumber;

        return $this;
    }

    /**
     * Get postNumber
     *
     * @return int
     */
    public function getPostNumber()
    {
        return $this->postNumber;
    }

    /**
     * Set postOffice
     *
     * @param string $postOffice
     *
     * @return Company
     */
    public function setPostOffice($postOffice)
    {
        $this->postOffice = $postOffice;

        return $this;
    }

    /**
     * Get postOffice
     *
     * @return string
     */
    public function getPostOffice()
    {
        return $this->postOffice;
    }

    /**
     * Set taxNumber
     *
     * @param integer $taxNumber
     *
     * @return Company
     */
    public function setTaxNumber($taxNumber)
    {
        $this->taxNumber = $taxNumber;

        return $this;
    }

    /**
     * Get taxNumber
     *
     * @return int
     */
    public function getTaxNumber()
    {
        return $this->taxNumber;
    }

    /**
     * Set isTaxPayer
     *
     * @param boolean $isTaxPayer
     *
     * @return Company
     */
    public function setIsTaxPayer($isTaxPayer)
    {
        $this->isTaxPayer = $isTaxPayer;

        return $this;
    }

    /**
     * Get isTaxPayer
     *
     * @return bool
     */
    public function getIsTaxPayer()
    {
        return $this->isTaxPayer;
    }

    /**
     * Set typeOfCompany
     *
     * @param string $typeOfCompany
     *
     * @return Company
     */
    public function setTypeOfCompany($typeOfCompany)
    {
        $this->typeOfCompany = $typeOfCompany;

        return $this;
    }

    /**
     * Get typeOfCompany
     *
     * @return string
     */
    public function getTypeOfCompany()
    {
        return $this->typeOfCompany;
    }
}

